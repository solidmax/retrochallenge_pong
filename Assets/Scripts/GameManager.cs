﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    GameManager instance; 

    public GameObject PlayerPrefab;
    public GameObject BallPrefab;

    public float ballSpeed;
    public float playerSpeed;

    public Transform player1StartPosition;
    public Transform player2StartPosition;
    public Transform ballStartPosition;

    PlayerController m_player1Controller;
    PlayerController m_player2Controller;

    private void Awake()
    {
        CreateSingleton();
    }

    void Start()
    {
       /* m_player1Controller = Instantiate(PlayerPrefab, player1StartPosition).GetComponent<PlayerController>();
        m_player2Controller = Instantiate(PlayerPrefab, player2StartPosition).GetComponent<PlayerController>();
        Instantiate(BallPrefab, ballStartPosition);
        */
    }

    void Update()
    {
        
    }

    void CreateSingleton()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
