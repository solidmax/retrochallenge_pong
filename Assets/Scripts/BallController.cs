﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float Speed = 3f;
    private Rigidbody2D rb; 

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        StartWithRandomMovement();
    }

    void Update()
    {
        
    }

    public void RestartBall()
    {
        rb.velocity = Vector2.zero; 
    }

    public void StartWithRandomMovement()
    {
        Vector2 tempVel; 
        tempVel.x = Random.Range(0, Speed);
        tempVel.y = Random.Range(0, Speed/2);

        rb.velocity = tempVel; 
    }

    public void SetVelocity(Vector2 newVel)
    {
        rb.velocity = newVel;
    }
}
