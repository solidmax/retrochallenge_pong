﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum PlayerNumber
    {
        Player1,
        Player2
    }
    public PlayerNumber Player; 

    public float Speed;
    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Move();
    }
   
    void Move()
    {
        float direction = Input.GetAxisRaw("Vertical");
        rb.MovePosition(new Vector2(0,transform.position.y + (Speed * direction * Time.deltaTime)));
        //transform.Translate(new Vector2(0, Speed * direction * Time.deltaTime));
    }
}
